# README #

Dramafever ios developer interview task

### How I build this app ###

JSONPlaceholder provides several API endpoints. I decide to use the /photo which has the most fake data entries (5000), it also has image url that I can play some async image download sdk. I decide to build a tableview which is often used in ios development. I listed several tech memo below:

* 1, FakeJson-ios-client is written in swift 3 
* 2, I use NSURLSession & JSONSerialization to fetch the data from API ([https://jsonplaceholder.typicode.com/photos](Link URL)). I didn't use other wrapper like alamofire since the API data is simple.
* 3, I created photo model to store each photo object 
* 4, In order to show web image into tableview, I used Kingfisher wrapper for web image download and cache. 
* 5, I use NVActivityIndicatorView to show a waiting indicator, although I can bridge-header the MBProgressHUD I used back in Objective C.

This is the screenshot of the app:
![Simulator Screen Shot Jun 12, 2017, 10.34.48 AM.png](https://bitbucket.org/repo/da6Kg99/images/3647487757-Simulator%20Screen%20Shot%20Jun%2012,%202017,%2010.34.48%20AM.png)

Thank you for read this!