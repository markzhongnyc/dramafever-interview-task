//
//  PhotoTableViewCell.swift
//  FakeJSON-IOS-Client
//
//  Created by Mark Zhong on 6/11/17.
//  Copyright © 2017 Mark Zhong. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var placeimg: UIImageView!
    @IBOutlet weak var albumid: UILabel!
    @IBOutlet weak var url: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
