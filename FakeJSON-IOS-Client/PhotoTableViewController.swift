//
//  PhotoTableViewController.swift
//  FakeJSON-IOS-Client
//
//  Created by Mark Zhong on 6/11/17.
//  Copyright © 2017 Mark Zhong. All rights reserved.
//

import UIKit
import Kingfisher
import NVActivityIndicatorView

class PhotoTableViewController: UITableViewController {
    var photoArr = [Photos]()
    let waitingView = NVActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.size.width*0.5-30,y: UIScreen.main.bounds.size.height*0.5-160, width: 60, height: 60), type:.ballSpinFadeLoader, color:UIColor.purple)



    override func viewDidLoad() {
        super.viewDidLoad()
        
        waitingView.startAnimating()
        self.view.addSubview(waitingView)
        
        self.getPhotoJson()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.photoArr.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "photoCell", for: indexPath) as! PhotoTableViewCell
        
        // Configure the cell...
        let photo = self.photoArr[indexPath.row]
        cell.title.text = "Title: " + photo.title
        cell.albumid.text = "ID:" + String(photo.id)
        cell.url.text = "URL: " + photo.url
        let url = URL(string: photo.thumbnailUrl)!
        
        //using Kingfisher image async download and cache
        cell.placeimg.kf.setImage(with: url, placeholder: UIImage(named:"defaultPhoto.png"))
        
        
        return cell
    }

    

    //MARK: -fetch JSON data
    
    func getPhotoJson(){
        
        let photoEndpoint: String = "https://jsonplaceholder.typicode.com/photos"
        guard let url = URL(string: photoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /photos")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let photo = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [[String: Any]] else {
                        print("error trying to convert data to JSON")
                        return
                }
                // now we have the photo
                
                for element in photo {
                    
                    //print("\(index) : \(element) \n")
                    
                    guard let albumId = element["albumId"] as? Int,
                        let title = element["title"] as? String,
                        let id = element["id"] as? Int,
                        let url = element["url"] as? String,
                        let thumbnailUrl = element["thumbnailUrl"] as? String
                        else {
                            print("error assign json value to photo model")
                            return
                    }
                    
                    guard let newPhoto = Photos(albumId: albumId, title: title, id: id, url: url, thumbnailUrl: thumbnailUrl) else {
                        fatalError("Unable to instantiate photo")
                    }
                    
                    self.photoArr += [newPhoto]
                }
                
                // Update UI in main queue
                DispatchQueue.main.async{
                    self.tableView.reloadData()
                    self.waitingView.stopAnimating()

                }
                
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
        
        
    }
   
}
